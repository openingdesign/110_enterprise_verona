\contentsline {section}{\numberline {1}Introduction and scope}{2}{section.1}%
\contentsline {section}{\numberline {2}Building codes}{2}{section.2}%
\contentsline {section}{\numberline {3}Loading criteria}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Gravity loading}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Wind design criteria}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Snow loading}{4}{subsection.3.3}%
\contentsline {section}{\numberline {4}Materials}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}Structural steel}{4}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Wood structural panels}{4}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Laminated veneer lumber (LVL)}{4}{subsubsection.4.2.1}%
\contentsline {section}{\numberline {5}Design and analysis software}{4}{section.5}%
\contentsline {section}{\numberline {6}Load combinations}{5}{section.6}%
\contentsline {section}{\numberline {7}Gravity}{5}{section.7}%
\contentsline {subsection}{\numberline {7.1}Change of occupation}{5}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Second floor headers}{5}{subsection.7.2}%
\contentsline {subsubsection}{\numberline {7.2.1}Headers at axes B and C}{5}{subsubsection.7.2.1}%
\contentsline {paragraph}{Structural design of the header.}{5}{section*.2}%
\contentsline {subparagraph}{Design load.}{5}{equation.7.1}%
\contentsline {subparagraph}{Internal forces.}{5}{section*.4}%
\contentsline {subparagraph}{Structural bending check.}{6}{section*.5}%
\contentsline {subparagraph}{Structural shear check.}{6}{section*.6}%
\contentsline {subparagraph}{Bending stiffness.}{6}{section*.7}%
\contentsline {subsubsection}{\numberline {7.2.2}Header over the door}{6}{subsubsection.7.2.2}%
\contentsline {paragraph}{Structural design of the header.}{6}{section*.8}%
\contentsline {subparagraph}{Design load.}{6}{equation.7.7}%
\contentsline {subparagraph}{Internal forces.}{7}{section*.10}%
\contentsline {subparagraph}{Structural bending check.}{7}{section*.11}%
\contentsline {subparagraph}{Structural shear check.}{7}{section*.12}%
\contentsline {subparagraph}{Bending stiffness.}{7}{section*.13}%
\contentsline {subsubsection}{\numberline {7.2.3}Stair header over the door}{7}{subsubsection.7.2.3}%
\contentsline {paragraph}{Structural design of the header.}{7}{section*.14}%
\contentsline {subparagraph}{Design load.}{7}{section*.15}%
\contentsline {subparagraph}{Internal forces.}{8}{section*.16}%
\contentsline {subparagraph}{Structural bending check.}{8}{section*.17}%
\contentsline {subparagraph}{Structural shear check.}{8}{section*.18}%
\contentsline {subparagraph}{Bending stiffness.}{8}{section*.19}%
\contentsline {paragraph}{Fire design of the header.}{8}{section*.20}%
\contentsline {subparagraph}{Mechanical properties of the burned section.}{8}{section*.21}%
\contentsline {subparagraph}{Structural bending check.}{8}{section*.22}%
\contentsline {subparagraph}{Structural shear check.}{9}{section*.23}%
\contentsline {subsection}{\numberline {7.3}Balcony extension}{9}{subsection.7.3}%
\contentsline {subsubsection}{\numberline {7.3.1}Joists}{9}{subsubsection.7.3.1}%
\contentsline {paragraph}{Structural design of joist.}{9}{section*.24}%
\contentsline {subparagraph}{Design load.}{9}{equation.7.25}%
\contentsline {subparagraph}{Internal forces.}{9}{section*.26}%
\contentsline {subparagraph}{Structural bending check.}{9}{section*.27}%
\contentsline {subparagraph}{Structural shear check.}{10}{section*.28}%
\contentsline {subparagraph}{Bending stiffness.}{10}{section*.29}%
\contentsline {subsubsection}{\numberline {7.3.2}Balcony beam}{10}{subsubsection.7.3.2}%
\contentsline {paragraph}{Structural design of the beam.}{10}{section*.30}%
\contentsline {subparagraph}{Design load.}{10}{equation.7.31}%
\contentsline {subparagraph}{Internal forces.}{10}{section*.32}%
\contentsline {subparagraph}{Structural bending check.}{10}{section*.33}%
\contentsline {subparagraph}{Structural shear check.}{10}{section*.34}%
\contentsline {subparagraph}{Bending stiffness.}{10}{section*.35}%
\contentsline {subsubsection}{\numberline {7.3.3}Balcony posts}{10}{subsubsection.7.3.3}%
\contentsline {paragraph}{Structural design of the posts.}{10}{section*.36}%
\contentsline {subsubsection}{\numberline {7.3.4}Balcony foundation}{11}{subsubsection.7.3.4}%
\contentsline {subsection}{\numberline {7.4}Main entrance steel beam}{11}{subsection.7.4}%
\contentsline {subsubsection}{\numberline {7.4.1}Structural design of the beam.}{11}{subsubsection.7.4.1}%
\contentsline {paragraph}{Design loads.}{11}{section*.37}%
\contentsline {paragraph}{Internal forces.}{14}{section*.38}%
\contentsline {paragraph}{Structural bending check.}{14}{section*.39}%
\contentsline {paragraph}{Structural shear check.}{14}{section*.40}%
\contentsline {paragraph}{Bending stiffness.}{14}{section*.41}%
\contentsline {subsection}{\numberline {7.5}Roof beam}{14}{subsection.7.5}%
\contentsline {subsubsection}{\numberline {7.5.1}Structural design of the beam.}{14}{subsubsection.7.5.1}%
\contentsline {paragraph}{Design loads.}{14}{section*.42}%
\contentsline {paragraph}{Internal forces.}{14}{section*.43}%
\contentsline {paragraph}{Structural bending check.}{14}{section*.44}%
\contentsline {paragraph}{Structural shear check.}{19}{section*.45}%
\contentsline {paragraph}{Bending stiffness.}{19}{section*.46}%
\contentsline {section}{\numberline {8}Lateral. Diaphragms/Shear walls}{19}{section.8}%
